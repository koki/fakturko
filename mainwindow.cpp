#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "dialog.h"
#include "firma.h"
#include "novafaktura.h"

#include <QFile>
#include <QStandardPaths>
#include <QDir>

#include <QStandardItemModel>
#include <QStandardItem>

#include <QJsonParseError>
#include <QMessageBox>

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    _posledenBroj = 0;

    const QString appDir = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation) + "/site_fakturi/";
    QDir dir(appDir);
    if (!dir.exists())
        dir.mkpath(".");

    QString filename = appDir + "data.json";
    QFile loadFile( filename );
    if (loadFile.open(QIODevice::ReadOnly))
    {
        QByteArray saveData = loadFile.readAll();
        QJsonParseError *error = new QJsonParseError();
        QJsonDocument loadDoc( QJsonDocument::fromJson(saveData, error) );
        if (loadDoc.isObject())
            momentalniPodatoci = loadDoc.object();
    }

    connect(ui->savePushButton, SIGNAL(clicked()),
            this, SLOT(zacuvajPodatoci()));
    connect(ui->firmaComboBox, SIGNAL(activated(int)),
            this, SLOT(odberiFirma(int)));

    connect(ui->novaFirmaPushButton, SIGNAL(clicked()),
            this, SLOT(dodadiNovaFirma()));
    connect(ui->novaFakturaPushButton, SIGNAL(clicked()),
            this, SLOT(dodadiNovaFaktura()));
    connect(ui->platiPushButton, SIGNAL(clicked()),
            this, SLOT(platiFaktura()));
    connect(ui->delButton, SIGNAL(clicked()),
            this, SLOT(izbrsiFaktura()));

    ui->firmaComboBox->addItem("site firmi ...", "ALLCOMP");
    napolniTabela();
    namestiSirina();
}

void MainWindow::napolniTabela()
{
    QString currentUuid = ui->firmaComboBox->currentData().toString();

    int plateno = 0;
    int nePlateno = 0;

    ui->firmaComboBox->clear();
    ui->firmaComboBox->addItem("site firmi ...", "ALLCOMP");

    QStandardItemModel *model = new QStandardItemModel(0, 4, this);
    model->setHorizontalHeaderLabels(QStringList() << "Broj" << "Firma" << "Datum" << "Iznos"<<"Status");

    QJsonObject::iterator i;
    for (i = momentalniPodatoci.begin(); i != momentalniPodatoci.end(); ++i)
    {
        QJsonObject firmaObject = i.value().toObject();
        QString ime = firmaObject["ime"].toString();
        QString firmaUuid = firmaObject["uuid"].toString();

        if ((currentUuid == "ALLCOMP" || currentUuid == firmaUuid) && firmaObject.contains("fakturi"))
        {
            QJsonObject fakturi = firmaObject["fakturi"].toObject();

            QJsonObject::iterator f;
            for (f=fakturi.begin(); f != fakturi.end(); ++f)
            {
                QJsonObject fakturaObject = f.value().toObject();

                QList<QStandardItem*> novaFaktura;
                int broj = fakturaObject["broj"].toInt();
                if (broj>_posledenBroj)
                    _posledenBroj = broj;

                QStandardItem *itemBroj = new QStandardItem(QString("%1").arg(broj, 8, 'g', -1, '0'));
                itemBroj->setData(fakturaObject["uuid"].toString(), Qt::UserRole);

                novaFaktura.append(itemBroj);
                novaFaktura.append(new QStandardItem(ime));
                novaFaktura.append(new QStandardItem(fakturaObject["datum"].toString()));

                int izn = fakturaObject["iznos"].toInt();
                QString iznos = QString::number(izn);
                bool platena = fakturaObject["plateno"].toBool();

                novaFaktura.append(new QStandardItem(iznos));
                if (platena){
                    plateno += izn;
                    novaFaktura.append(new QStandardItem("plateno"));
                }
                else{
                    nePlateno += izn;
                    novaFaktura.append(new QStandardItem("dolzi"));
                }

                model->appendRow(novaFaktura);
            }
        }

        _siteFirmi[firmaUuid] = QVariant(ime);
        ui->firmaComboBox->addItem(ime, firmaUuid);
    }

    ui->platenoLabel->setText(QString::number(plateno));
    ui->nePlatenoLabel->setText(QString::number(nePlateno));
    ui->vkupnoLabel->setText(QString::number(plateno+nePlateno));

    ui->tableView->setModel(model);
    ui->tableView->setContentsMargins(20,5,20,5);
}

void MainWindow::odberiFirma(int index)
{
    napolniTabela();
    ui->firmaComboBox->setCurrentIndex(index);
}

void MainWindow::dodadiNovaFirma()
{
    Dialog *one = new Dialog(this);
    one->setModal(true);
    if (one->exec())
    {
        Firma nova = one->getFirmaData();
        momentalniPodatoci.insert(nova.getUuid(), nova.getAsJsonValue());
    }
    one->deleteLater();

    napolniTabela();
}

void MainWindow::dodadiNovaFaktura()
{
    NovaFaktura *one = new NovaFaktura(this);
    one->setModal(true);
    one->napolniFirmi(_siteFirmi/*, _posledenBroj*/);
    if (one->exec())
    {
        QList<Faktura> fakturi = one->gotoviFakturi();
        foreach(Faktura faktura, fakturi)
        {
           faktura.setBroj(++_posledenBroj);
           QJsonValue val = faktura.getAsJsonValue();
           QString uuid = faktura.getUuid();

           QJsonObject obj = momentalniPodatoci[faktura.getParentUuid()].toObject();
           QJsonObject stariFakturi;
           if (obj.contains("fakturi"))
               stariFakturi = obj["fakturi"].toObject();
           stariFakturi.insert(uuid, val);
           obj.insert("fakturi", QJsonValue(stariFakturi));
           momentalniPodatoci.insert(faktura.getParentUuid(), QJsonValue(obj));
        }

        napolniTabela();
    }
    one->deleteLater();
}

void MainWindow::zacuvajPodatoci()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Zacuvaj", "Dali ste sigurni deka sakate da ja zacuvate ovaa sostojba?",
                                  QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::No)
      return;

    const QString appDir = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation) + "/site_fakturi/";
    QDir dir(appDir);
    if (!dir.exists()) {
        dir.mkpath(".");
    }

    QString filename = appDir + "data.json";
    QFile saveFile(filename);

    if (!saveFile.open(QIODevice::WriteOnly)) {
        qWarning("Couldn't open save file.");
        return;
    }

    QJsonDocument saveDoc(momentalniPodatoci);
    saveFile.write(saveDoc.toJson());
}

void MainWindow::platiFaktura()
{
    QModelIndexList sel = ui->tableView->selectionModel()->selectedRows();
    foreach(QModelIndex index, sel)
    {
        QString uuidTable = ui->tableView->model()->itemData(index)[Qt::UserRole].toString();
        QJsonObject::iterator i;
        for (i = momentalniPodatoci.begin(); i != momentalniPodatoci.end(); ++i)
        {
            QJsonObject firmaObject = i.value().toObject();
            QString firmaUuid = firmaObject["uuid"].toString();

            if (firmaObject.contains("fakturi"))
            {
                QJsonObject fakturi = firmaObject["fakturi"].toObject();
                QJsonObject::iterator f;
                for (f=fakturi.begin(); f != fakturi.end(); ++f)
                {
                    QJsonObject fakturaObj = f.value().toObject();
                    QString uuid = fakturaObj["uuid"].toString();

                    if (uuidTable == uuid)
                    {
                        fakturaObj.insert("plateno", true);
                        fakturi.insert(uuid, QJsonValue(fakturaObj));
                        firmaObject.insert("fakturi", QJsonValue(fakturi));
                        momentalniPodatoci.insert(firmaUuid, QJsonValue(firmaObject));

                        break;
                    }
                }
            }
        }
    }

    napolniTabela();
}

void MainWindow::izbrsiFaktura()
{
    QModelIndexList sel = ui->tableView->selectionModel()->selectedRows();
    foreach(QModelIndex index, sel)
    {
        QString uuidTable = ui->tableView->model()->itemData(index)[Qt::UserRole].toString();
        QJsonObject::iterator i;
        for (i = momentalniPodatoci.begin(); i != momentalniPodatoci.end(); ++i)
        {
            QJsonObject firmaObject = i.value().toObject();
            QString firmaUuid = firmaObject["uuid"].toString();

            if (firmaObject.contains("fakturi"))
            {
                QJsonObject fakturi = firmaObject["fakturi"].toObject();

                if (fakturi.contains(uuidTable))
                {
                    fakturi.remove(uuidTable);
                    firmaObject.insert("fakturi", QJsonValue(fakturi));
                    momentalniPodatoci.insert(firmaUuid, QJsonValue(firmaObject));
                }
            }
        }
    }

    napolniTabela();
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    namestiSirina();
}

void MainWindow::namestiSirina()
{
    int wdt = ui->tableFrame->width();

    ui->tableView->setColumnWidth(0, wdt/6-5);
    ui->tableView->setColumnWidth(1, wdt/3-5);
    ui->tableView->setColumnWidth(2, wdt/6-5);
    ui->tableView->setColumnWidth(3, wdt/6-5);
    ui->tableView->setColumnWidth(4, wdt/6-5);
}

MainWindow::~MainWindow()
{
    delete ui;
    //delete _posledenBroj;
}
