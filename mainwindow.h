#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>

#include <QVariantMap>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void napolniTabela();
    void namestiSirina();

    ~MainWindow();

private slots:
    void zacuvajPodatoci();
    void dodadiNovaFirma();
    void dodadiNovaFaktura();
    void platiFaktura();
    void odberiFirma(int index);
    void izbrsiFaktura();

protected:
    void resizeEvent(QResizeEvent *event);

private:
    Ui::MainWindow *ui;
    QJsonObject momentalniPodatoci;
    QVariantMap _siteFirmi;
    int _posledenBroj;
};

#endif // MAINWINDOW_H
