#include "novafaktura.h"
#include "ui_novafaktura.h"

NovaFaktura::NovaFaktura(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NovaFaktura)
{
    ui->setupUi(this);

    ui->dateEdit->setCalendarPopup(true);
    connect(ui->addButton, SIGNAL(clicked()),
            this, SLOT(dodadiNovProizvod()));
}

void NovaFaktura::napolniFirmi(QVariantMap firmi/*, int *posledenBroj*/)
{
    //_posledenBroj = posledenBroj;

    for(QVariantMap::const_iterator iter = firmi.begin(); iter != firmi.end(); ++iter)
    {
        ui->firmaComboBox->addItem(iter.value().toString(), iter.key());
    }

    ui->dateEdit->setDate(QDate::currentDate());
    dodadiNovProizvod();
}

void NovaFaktura::dodadiNovProizvod()
{
    //QHBoxLayout *l = new QHBoxLayout();
    QFrame *frame = new QFrame(this);

    QLineEdit *naziv = new QLineEdit(frame);
    QDoubleSpinBox *cena = new QDoubleSpinBox(frame);

    cena->setMinimumWidth(100);
    cena->setMaximumWidth(100);
    cena->setMinimum(0.00);
    cena->setMaximum(9999999999);
    cena->setSingleStep(1.00);

    _siteCeni<<cena;
    _siteProizvodi<<naziv;

    connect(cena, SIGNAL(valueChanged(double)),
            this, SLOT(updateVkupno(double)));

    frame->setContentsMargins(0,0,0,0);
    frame->setLayout(new QHBoxLayout());
    frame->layout()->addWidget(naziv);
    frame->layout()->addWidget(cena);

    ui->proizvodiVerticalLayout->addWidget(frame);
}

void NovaFaktura::updateVkupno(double vkupno)
{
    double suma = 0.00;

    foreach(QDoubleSpinBox *spin, _siteCeni)
        if(spin->value()>0)
            suma += spin->value();

    ui->vkupnoDoubleSpinBox->setValue(suma);
}

QList<Faktura> NovaFaktura::gotoviFakturi()
{
    QList<Faktura> list;
    QJsonObject proizvodi;

    for(int i = 0; i < _siteCeni.length(); i++)
    {
        if (_siteProizvodi[i]->text() != "")
        {
            QJsonObject p;
            p.insert("naziv", _siteProizvodi[i]->text());
            p.insert("cena", _siteCeni[i]->value());

            proizvodi.insert(QString::number(i), QJsonValue(p));
        }
    }

    //++*_posledenBroj;
    QUuid uuid = QUuid(ui->firmaComboBox->currentData().toString());
    //Faktura nova = Faktura(uuid, proizvodi, *_posledenBroj);
    Faktura nova = Faktura(uuid, proizvodi);
    list<<nova;


    return list;
}

NovaFaktura::~NovaFaktura()
{
    delete ui;
    //delete _posledenBroj;
}
