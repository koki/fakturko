#ifndef DIALOG_H
#define DIALOG_H

#include "firma.h"
#include <QDialog>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);

    Firma getFirmaData();

    ~Dialog();

protected:
    void accept();

private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H
