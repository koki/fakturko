#ifndef FIRMA_H
#define FIRMA_H

#include <QUuid>
#include <QJsonObject>
#include <QDate>

class Firma
{
public:
    Firma();
    Firma(const QJsonObject &jsonObj);// kreiraj nov objekt od JSON
    Firma(const QString &ime, const QString &adresa, const QString &drugo);//kreiraj nov objekt od podatoci NOTE: tuka UUID se generira

    QJsonObject getAsJsonObject();
    QJsonValue getAsJsonValue();
    QString getUuid();

    ~Firma();
private:
    QString _ime;
    QString _adresa;
    QString _drugiPodatoci;

    QUuid _uuid;
    QJsonObject _jsonData;
};

class Faktura : public Firma
{
public:
    Faktura();
    Faktura(const QJsonObject &jsonObj);// kreiraj nov objekt od JSON
    Faktura(QUuid parent, QJsonObject proizvodi);//kreiraj nov objekt od podatoci NOTE: tuka UUID se generira

    QJsonObject getAsJsonObject();
    QJsonValue getAsJsonValue();
    QString getUuid();
    QString getParentUuid();
    void platiFaktura();
    void setBroj(int broj);

    ~Faktura();
private:
    QDate _datum;
    int _iznos;
    bool _plateno;
    int _seriskiBroj;

    QUuid _uuid;
    QUuid _firmaUuid;
    QJsonObject _proizvodi;
    QJsonObject _jsonData;
};

#endif // FIRMA_H
