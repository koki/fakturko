#-------------------------------------------------
#
# Project created by QtCreator 2015-04-24T14:17:35
#
#-------------------------------------------------

QT       += xml core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = invoice_tracker
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    firma.cpp \
    dialog.cpp \
    novafaktura.cpp

HEADERS  += mainwindow.h \
    firma.h \
    dialog.h \
    novafaktura.h

FORMS    += mainwindow.ui \
    dialog.ui \
    novafaktura.ui
