#include "dialog.h"
#include "ui_dialog.h"

#include <QDebug>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

void Dialog::accept()
{
    if(ui->imeLineEdit->text() == "")
    {
        QDialog::reject();
        return;
    }

    QDialog::accept();
}

Firma Dialog::getFirmaData()
{
    QString ime = ui->imeLineEdit->text();
    QString adresa = ui->adresaEdit->toPlainText();
    QString drugo = ui->drugoEdit->toPlainText();

    return Firma(ime, adresa, drugo);
}

Dialog::~Dialog()
{
    delete ui;
}
