#ifndef NOVAFAKTURA_H
#define NOVAFAKTURA_H

#include "firma.h"

#include <QDialog>
#include <QCheckBox>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QDebug>
#include <QDoubleSpinBox>

namespace Ui {
class NovaFaktura;
}

class NovaFaktura : public QDialog
{
    Q_OBJECT

public:
    explicit NovaFaktura(QWidget *parent = 0);

    void napolniFirmi(QVariantMap firmi/*, int *posledenBroj*/);
    QList<Faktura> gotoviFakturi();

    ~NovaFaktura();

public slots:
    void dodadiNovProizvod();
    void updateVkupno(double vkupno);

private:
    QList<QDoubleSpinBox *> _siteCeni;
    QList<QLineEdit *> _siteProizvodi;
    QList<QCheckBox *> _siteFirmi;
    Ui::NovaFaktura *ui;
    //int *_posledenBroj;
};

#endif // NOVAFAKTURA_H
