#include "firma.h"

Firma::Firma()
{
    _ime = "";
    _adresa = "";
    _drugiPodatoci = "";

    _uuid = QUuid();

    _jsonData = QJsonObject();
}

Firma::Firma(const QJsonObject &jsonObj)
{
    _ime = jsonObj["ime"].toString();
    _adresa = jsonObj["adresa"].toString();
    _drugiPodatoci = jsonObj["drugo"].toString();

    _uuid = QUuid( jsonObj["uuid"].toString() );

    _jsonData = jsonObj;
}

Firma::Firma(const QString &ime, const QString &adresa, const QString &drugo)
{
    _ime = ime;
    _adresa = adresa;
    _drugiPodatoci = drugo;

    _uuid = QUuid::createUuid();

    _jsonData.insert("ime", _ime);
    _jsonData.insert("adresa", _adresa);
    _jsonData.insert("drugo", _drugiPodatoci);
    _jsonData.insert("uuid", _uuid.toString());
}

QJsonObject Firma::getAsJsonObject()
{
   return _jsonData;
}

QJsonValue Firma::getAsJsonValue()
{
    return QJsonValue(_jsonData);
}

QString Firma::getUuid()
{
    return _uuid.toString();
}

Firma::~Firma()
{

}

Faktura::Faktura()
{
    _datum = QDate();
    _iznos = 0;
    _plateno = false;
    _seriskiBroj = 0;

    _uuid = QUuid();
    _firmaUuid = QUuid();
    _proizvodi = QJsonObject();
    _jsonData = QJsonObject();
}

Faktura::Faktura(const QJsonObject &jsonObj)
{
}

Faktura::Faktura(QUuid parent, QJsonObject proizvodi)
{
    _firmaUuid = parent;
    _proizvodi = proizvodi;
    _plateno = false;
    _seriskiBroj = 0;

    _datum = QDate::currentDate();
    _uuid = QUuid::createUuid();
    _iznos = 0;

    QJsonObject::iterator i;
    for (i = _proizvodi.begin(); i != _proizvodi.end(); ++i)
    {
        QJsonObject jo = i.value().toObject();
        int cena = jo["cena"].toInt();
        if (cena > 0)
            _iznos += cena;
    }

    _jsonData.insert("datum", _datum.toString());
    _jsonData.insert("plateno", _plateno);
    _jsonData.insert("iznos", _iznos);

    _jsonData.insert("firmaUuid", _firmaUuid.toString());
    _jsonData.insert("uuid", _uuid.toString());
    _jsonData.insert("proizvodi", _proizvodi);
}

void Faktura::platiFaktura()
{
    if (!_plateno)
        _plateno = true;
}

void Faktura::setBroj(int broj)
{
    _seriskiBroj = broj;
    _jsonData.insert("broj", _seriskiBroj);
}

QJsonObject Faktura::getAsJsonObject()
{
   return _jsonData;
}

QJsonValue Faktura::getAsJsonValue()
{
    return QJsonValue(_jsonData);
}

QString Faktura::getParentUuid()
{
    return _firmaUuid.toString();
}

QString Faktura::getUuid()
{
    return _uuid.toString();
}

Faktura::~Faktura()
{

}

